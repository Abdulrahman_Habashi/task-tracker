'use strict';


/**
 * Add a new task to the store
 *
 * body TaskCreateRequest Task object that needs to be added to the store
 * returns Task
 **/
exports.addTask = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "longDescription" : "longDescription",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "createdBy" : "createdBy",
  "id" : 0,
  "lastUpdateAt" : "2000-01-23T04:56:07.000+00:00",
  "title" : "title",
  "lastUpdateBy" : "lastUpdateBy",
  "status" : "status"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete task by id
 *
 * id Integer Task id
 * no response value expected for this operation
 **/
exports.deleteTaskById = function(id) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 *
 * id Integer Task id
 * returns Task
 **/
exports.getTaskById = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "longDescription" : "longDescription",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "createdBy" : "createdBy",
  "id" : 0,
  "lastUpdateAt" : "2000-01-23T04:56:07.000+00:00",
  "title" : "title",
  "lastUpdateBy" : "lastUpdateBy",
  "status" : "status"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all tasks
 *
 * returns TasksResponse
 **/
exports.getTasks = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "longDescription" : "longDescription",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "createdBy" : "createdBy",
  "id" : 0,
  "lastUpdateAt" : "2000-01-23T04:56:07.000+00:00",
  "title" : "title",
  "lastUpdateBy" : "lastUpdateBy",
  "status" : "status"
}, {
  "longDescription" : "longDescription",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "createdBy" : "createdBy",
  "id" : 0,
  "lastUpdateAt" : "2000-01-23T04:56:07.000+00:00",
  "title" : "title",
  "lastUpdateBy" : "lastUpdateBy",
  "status" : "status"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Update an existing task
 *
 * body Task Task object that needs to be added to the store
 * no response value expected for this operation
 **/
exports.updateTask = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

